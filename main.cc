#include <functional>
#include <iostream>
#include <memory>
#include <vector>

template <typename Func, typename... Args>
inline std::pair<Func, std::tuple<Args...>> MakeFilter(Func func, Args... args) {
	return std::make_pair(func, std::make_tuple(args...));
}

template <typename T>
bool ApplyFilter(const T&) {
	return true;
}

template <typename T, typename Filter>
bool ApplyFilter(const T& elem, Filter filter);

template <typename T, typename Filter, typename... Filters>
bool ApplyFilter(const T& elem, Filter filter, Filters... filters);

#ifdef OLD_CPP /* C++11, C++14 ; gcc < 7.0 */

// 0 args

template <typename T, typename Func>
bool ApplyFilter(const T& elem, const std::pair<Func, std::tuple<>>& filter) {
	auto f = std::bind(filter.first, elem);
	return f();
}

template <typename T, typename Func, typename... Filters>
bool ApplyFilter(const T& elem, const std::pair<Func, std::tuple<>>& filter, Filters... filters) {
	auto f = std::bind(filter.first, elem);
	return f() && ApplyFilter(elem, filters...);
}

// 1 args

template <typename T, typename Func, typename Arg1>
bool ApplyFilter(const T& elem, const std::pair<Func, std::tuple<Arg1>>& filter) {
	auto f = std::bind(filter.first, elem, std::get<0>(filter.second));
	return f();
}

template <typename T, typename Func, typename Arg1, typename... Filters>
bool ApplyFilter(const T& elem, const std::pair<Func, std::tuple<Arg1>>& filter, Filters... filters) {
	auto f = std::bind(filter.first, elem, std::get<0>(filter.second));
	return f() && ApplyFilter(elem, filters...);
}

// 2 args

template <typename T, typename Func, typename Arg1, typename Arg2>
bool ApplyFilter(const T& elem, const std::pair<Func, std::tuple<Arg1, Arg2>>& filter) {
	auto f = std::bind(filter.first, elem, std::get<0>(filter.second), std::get<1>(filter.second));
	return f();
}

template <typename T, typename Func, typename Arg1, typename Arg2, typename... Filters>
bool ApplyFilter(const T& elem, const std::pair<Func, std::tuple<Arg1, Arg2>>& filter, Filters... filters) {
	auto f = std::bind(filter.first, elem, std::get<0>(filter.second), std::get<1>(filter.second));
	return f() && ApplyFilter(elem, filters...);
}

#else /* C++17 ; gcc >= 7.0 */

template <typename T, typename Func, typename... Args>
bool ApplyFilter(const T& elem, const std::pair<Func, std::tuple<Args...>>& filter) {
	return std::apply(filter.first, std::tuple_cat(std::make_tuple(elem), filter.second));
}

template <typename T, typename Func, typename... Args, typename... Filters>
bool ApplyFilter(const T& elem, const std::pair<Func, std::tuple<Args...>>& filter, Filters... filters) {
	return std::apply(filter.first, std::tuple_cat(std::make_tuple(elem), filter.second))
		&& ApplyFilter(elem, filters...);
}

#endif

class Number;
using NumberPtr = std::shared_ptr<Number>;

class Number {
public:
	Number(int n) : n(n) {}
	bool IsEven() const { return n % 2 == 0; }
	bool IsPositive() const { return n > 0; }
	bool IsGreaterThen(int x) const { return n > x; }

	template <typename... Filters>
	static std::vector<NumberPtr> GetList(Filters... filters) {
		std::vector<NumberPtr> list;
		const std::vector<int> default_list { 1, 3, 2, 5, -4, 6 };

		for (auto x : default_list) {
			auto n = std::make_shared<Number>(x);
			if (ApplyFilter(n, filters...)) {
				list.push_back(n);
			}
		}

		return list;
	}

	static void Print(const std::vector<NumberPtr>& list) {
		for (const auto& elem : list) {
			std::cout << elem->n << " ";
		}
		std::cout << "\n";
	}

private:
	int n;
};

int main() {
	auto list = Number::GetList(MakeFilter(&Number::IsEven),
								MakeFilter(&Number::IsPositive),
								MakeFilter(&Number::IsGreaterThen, 1));
	Number::Print(list);
}
